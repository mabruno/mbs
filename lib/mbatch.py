import pickle
import os,sys 
from datetime import datetime

part=open('partition','r').readlines()[0].rsplit()
cn=[int(p) for p in part]
if cn[0]<=0:
    print 'Partition error = ', part
    sys.exit(1)
if cn[1]>=73:
    print 'Partition error = ', part
    sys.exit(1)

partition=range(cn[0],cn[1]+1)
DEBUG=False

###################

db={}
for i in partition:
    db[i] = 0

def refresh_db():
    running=open('running','r').readlines()
    done=[]
    for r in running:
        jname=r.split()[0]
        nodes=r.rsplit()[2]
        line=os.popen('tail -1 %s.out' % jname).read().rstrip()
        if (line=='JOB COMPLETED'):
            for e in nodes.split(','):
                db[int(e[2:])] = 0
            done+=[r]
        else:
            for e in nodes.split(','):
                db[int(e[2:])] = 1
    
    for d in done:
        running.remove(d)
    with open('running','w') as ff:
        for r in running:
            ff.write(r)
            

def get_nodes(fname):
    job=open(fname,'r').readlines()
    for line in job:
        if (line[0:6]=='#NODES'):
            h=line.split('=')
            return int(h[1].rstrip())

def free_nodes():
    ret=[]
    for i in partition:
        if (db[i]==0):
            ret+=[i]
    return ret

def submit_next():
    jlist=open('queue','r').readlines()

    submitted=[]
    for j in jlist:
        # rstrip removes trailing newline \n
        jname=j.rstrip()
        n=get_nodes(jname)
        
        free=free_nodes()

        # goes to next job in list if job is too big
        if (n>len(free)):
            continue

        nodes='cn%02d' % free[0]
        db[free[0]]=1
        for k in range(1,n):
            nodes += ',cn%02d' % free[k]
            db[free[k]]=1

        out=os.popen('nohup %s %s > %s.out &' % (jname, nodes, jname)).read()
        if (out!=''):
            print 'failed %s' % jname
        else:
            if DEBUG:
                print 'submmited %s %s \n' % (jname,nodes)
            submitted+=[j]
            with open('running','a') as ff:
                now = datetime.now().strftime("%d.%m.%Y-%H.%M.%S") 
                ff.write('%s %s %s\n' % (jname, now, nodes))

    # remove submitted jobs from queue
    for s in submitted:
        jlist.remove(s)
    with open('queue','w') as ff:
        for j in jlist:
            ff.write(j)


refresh_db()
submit_next()

