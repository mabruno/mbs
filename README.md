# MattiaBatchSystem mbs v0.2

Mattia's Batch System for the CERN TH hpc-qcd cluster.

## Installation

Clone the git repository and add the `bin` directory to the 
`PATH` environement variable

```bash
git clone git@gitlab.com:mabruno/mbs.git
export PATH=$PATH:/path/to/mbs/bin
mbs-reset
```

The `mbs-reset` command creates the necessary files before the first
usage and resets the entire scheduler in case of problems.

## Usage

First, define the partition of the machine assigned to the user to be used by the scheduler run

```bash
mbs-partition first_node last_node
```

To check what is the current partition used by the scheduler
```bash
$ mbs-partition
Current partition set to [first and last included]
9 1
```

To submit jobs (one at a time)

```bash
mbs-sub script.job
```

The job scheduler is refreshed by the command `mbs` which can be run 
from the terminal, which automatically checks the jobs that have been
terminated, removes them from the queue and executes the next job in the
queue that fits the available partition. The job scheduler can be automatized
with `acrontab`.

To check the queue and the running jobs

```bash
$ mbs-check

Jobs in the queue
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1240.job
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1260.job
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1280.job

Jobs runnig
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1200.job 17.06.2020-18.00.37 cn09,cn10,cn11,cn12,cn13,cn14,cn15,cn16,cn17,cn18,cn19,cn20,cn21,cn22,cn23,cn24
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1220.job 18.06.2020-00.00.19 cn25,cn26,cn27,cn28,cn29,cn30,cn31,cn32,cn33,cn34,cn35,cn36,cn37,cn38,cn39,cn40

```

When the job is run by the job scheduler an output file is automatically
generated with the standard output and error produced by the job script.

To kill a job, first check the job name using `mbs-check`, then run `mbs-kill`

```bash
$ mbs-check

Jobs in the queue
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1240.job
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1260.job
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1280.job

Jobs runnig
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1200.job 17.06.2020-18.00.37 cn09,cn10,cn11,cn12,cn13,cn14,cn15,cn16,cn17,cn18,cn19,cn20,cn21,cn22,cn23,cn24
/cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1220.job 18.06.2020-00.00.19 cn25,cn26,cn27,cn28,cn29,cn30,cn31,cn32,cn33,cn34,cn35,cn36,cn37,cn38,cn39,cn40

$ mbs-kill /cephfs/user/mabruno/runs/32IDfine/evecs/evecs-gen-1200.job
```

## Requirements: the job scripts

- at the beginning of the job script the varible `#NODE` must be defined
- the job script is run by the scheduler with one argument, therefore the line
  `NODES=$1:16` should be place in the job script and used by the `rmpi` command
- the job script must print *JOB COMPLETED* at the end of the job

## Example

Running a job on a single node with 16 ranks per node
```
#!/bin/bash
#NODES=1
NODES=$1:16
rmpi $NODES exe
echo "JOB COMPLETED"
exit 0
```

Running a job on 8 nodes with 8 ranks per node
```
#!/bin/bash
#NODES=8
NODES=$1:8

date

rmpi $NODES exe

date
echo "JOB COMPLETED"
exit 0
```
